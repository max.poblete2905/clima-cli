const inquirer = require('inquirer');
require('colors');

const menuOpt = [
    {
        type: 'list',
        name: 'opcion',
        message: '¿ que desea hacer ?',
        choices: [
            {
                value: 1,
                name: `${'1'.green}. Consultar ciudad`
            },
            {
                value: 2,
                name: `${'2.'.green} Historial`
            },
            {
                value: 0,
                name: `${'0.'.green} salir`
            }
        ],
    }
]

const inquirerMenu = async () => {
    console.clear()
    console.log('================================='.green)
    console.log('      IMFORMACION DEL CLIMA         '.yellow)
    console.log('=================================\n'.green)
    const { opcion } = await inquirer.prompt(menuOpt)
    return opcion
}

const pausa = async () => {
    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `\nPress ${'ENTER'.green}  to continue\n`
        }

    ]
    await inquirer.prompt(question)
}

const leerInput = async (message) => {
    const question = [
        {
            type: 'input',
            name: 'desc',
            message,
            validate(value) {
                if (value.length === 0) {
                    return 'por favor ingrese un valor ';
                }
                return true;
            }
        }
    ];
    const { desc } = await inquirer.prompt(question);
    return desc;
}

const listarLugares = async (lugares = []) => {

    const choices = lugares.map((lugar, i) => {
        const index = `${i + 1}`.green;
        return {
            value: lugar.id,
            name: `${index} ${lugar.nombre}`,
        }
    })
    choices.unshift({
        value: 0,
        name: `${'0'.green} ${'Cancelar'.red} `,
    })
    const preguntas = [
        {
            type: 'list',
            name: 'id',
            message: 'Seleccione lugar:',
            choices,
        }
    ]
    const { id } = await inquirer.prompt(preguntas);
    return id;
}

const confirmar = async (message) => {
    const pregunta = [
        {
            type: 'confirm',
            name: 'ok',
            message
        }
    ]
    const { ok } = await inquirer.prompt(pregunta)
    return ok;
}

const mostrarListadoCheclist = async (tareas = []) => {
    const choices = tareas.map((tarea, i) => {
        const index = `${i + 1}`.green;
        return {
            value: tarea.id,
            name: `${index} ${tarea.descripcion}`,
            checked: (tarea.completadoEn ? true : false),
        }
    })
    const preguntas = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Seleccione',
            choices,
        }
    ]
    const { ids } = await inquirer.prompt(preguntas);
    return ids;
}

module.exports = {
    inquirerMenu,
    pausa,
    leerInput,
    listarLugares,
    confirmar,
    mostrarListadoCheclist,
}
