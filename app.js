require('dotenv').config()
const { leerInput, inquirerMenu, pausa, listarLugares } = require("./helper/inquirer");
const Busqueda = require("./model/busqueda");
const { buscarCiudad } = require('./model/busqueda')

const main = async () => {

    const busquedas = new Busqueda();
    let opt = '';

    do {
        opt = await inquirerMenu();

        switch (opt) {
            case 1:
                //mostrar mensaje 
                const lugar = await leerInput('Ingrese Cuidad :');

                //buscar lugares
                const lugares = await busquedas.buscarCiudad(lugar);

                // selecciona lugar
                const idSelected = await listarLugares(lugares);
                if (idSelected === 0) continue;
                const lugarSelected = lugares.find(l => l.id === idSelected)

                busquedas.agregarHistorial(lugarSelected.nombre);

                // Clima
                const clima = await busquedas.climaLugar(lugarSelected.lat, lugarSelected.lng);

                //mostrar resultados
                const nomnreciudad = lugarSelected.nombre.split(',')

                // console.clear()

                console.log(`\nINFORMACION CLIMA ${nomnreciudad[0].toUpperCase()} \n`.green)
                console.log(`${'1.'.green} Ciudad      : ${lugarSelected.nombre}`)
                console.log(`${'2.'.green} Latitud     : ${lugarSelected.lng}`)
                console.log(`${'3.'.green} Longitud    : ${lugarSelected.lat}`)
                console.log(`${'4.'.green} Temperatura : ${clima.temp}`)
                console.log(`${'5.'.green} Minima      : ${clima.min}`)
                console.log(`${'6.'.green} Maxima      : ${clima.max}`)
                console.log(`${'7.'.green} descripcion : ${clima.desc}`)
                break;
            case 2:
                busquedas.historialPatitalizado.forEach((lugar, i) => {
                    const idx = `${i + 1}.`.green;
                    console.log(`${idx} ${lugar}`) 
                })
                break;
        }

        if (opt !== 0) await pausa();

    } while (opt !== 0)

}

main();
