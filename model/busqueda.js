const fs = require('fs')
const axios = require('axios');


class Busqueda {
    historial = []
    dbPaht = './db/dataBasa.json'

    constructor() {
        this.leerDB();
    }

    get historialPatitalizado() {
        return this.historial.map((lugar) => {

            let palabras = lugar.split(' ');
            palabras = palabras.map((p) => p[0].toUpperCase() + p.substring(1));

            return palabras.join(' ');
        })
    }

    get paramsMapbox() {
        return {
            'access_token': process.env.MAPBOX_KEY,
            'limit': 5,
            'language': 'es'
        }
    }
    async buscarCiudad(lugar = '') {
        try {
            const intance = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${lugar}.json`,
                params: this.paramsMapbox,
            })
            const resp = await intance.get();
            return resp.data.features.map(lugar => ({
                id: lugar.id,
                nombre: lugar.place_name,
                lng: lugar.center[0],
                lat: lugar.center[1],
            }));

        } catch (error) {
            return [];
        }
    }

    get parameterWatheropen() {
        return {
            appid: process.env.OPENWETHER_KEY,
            lang: 'es',
            units: 'metric',
        }
    }

    async climaLugar(lat, lon) {
        try {

            // instancia de axios
            const intancia = axios.create({
                baseURL: `https://api.openweathermap.org/data/2.5/weather`,
                params: { ...this.parameterWatheropen, lat, lon },
            })
            // resp.data
            const resp = await intancia.get();

            return {
                desc: resp.data.weather[0].description,
                min: resp.data.main.temp_min,
                max: resp.data.main.temp_max,
                temp: resp.data.main.temp,
            }

        } catch (error) {

            console.log(error);
        }

    }

    agregarHistorial(lugar = '') {

        // TODO:PREVENIR DUPLICADOS
        if (this.historial.includes(lugar.toLocaleLowerCase())) {
            return;
        }

        this.historial = this.historial.splice(0, 9);

        this.historial.unshift(lugar.toLocaleLowerCase())

        // grabar en db
        this.guardarDB()

    }

    guardarDB() {

        const payload = {
            historial: this.historial
        };

        fs.writeFileSync(this.dbPaht, JSON.stringify(payload))
    }

    leerDB() {

        if (!fs.existsSync(this.dbPaht)) {
            return;
        }
        const info = fs.readFileSync(this.dbPaht, { encoding: 'utf-8' })
        const data = JSON.parse(info);
        this.historial = data.historial
        console.log(data)
        // existir 

    }
}

module.exports = Busqueda;
